# Employee test

# swagger

swagger is configured and can be access via localhost:8080/swagger-ui.html
# postman
url localhost:8080/employee can be accessed from postman too.

#input for save

{
"employeeId": 0,
"firstName": "emp",
"lastName":"Test 222",
"email":"abc@testemp.com",
"phoneNumber":"22333-3333",
"hireDate":"2021-11-09T07:09:03.459+00:00",
"salary":12222,
"managerId": 3,
"departmentId":2
}

#for update

{
"employeeId":1,
"firstName": "emp",
"lastName":"Test 222",
"email":"abc@testemp.com",
"phoneNumber":"22333-3333",
"hireDate":"2021-11-09T07:09:03.459+00:00",
"salary":12222,
"managerId": 3,
"departmentId":2
}

#for delete

just pass employee id in field.

# Joins
Joins were not mentioned in script so I did not have implemented.
but relation of employee can be made by changing long departmentId to Department departmentId,
use @ManyToOne annotation above the field


## Authors and acknowledgment
Test and Free

## License
No 
